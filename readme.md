networkc
====

Network Library to calculate degree, closeness, betweenness centrality and sorts them in descending order

* Calculate Degree Centality
* Calculate Closeness Centrality
* Calculate Betweenness Centrality

This library -

1. Calculates one or more centrality measures from the above list
2. Sorts them in descending order of centrality
3. Saves the node ids and corresponding centrality measure in a csv


##Testing Instructions

To test this library use the following command on terminal :
`python networkc/networkc.test.py`
